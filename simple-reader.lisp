(defpackage :generic-reader.simple-reader
    (:use :cl :generic-reader)
  (:export
   #:simple-reader
   ;; generic functions
   #:simple-reader-char-syntax
   #:simple-reader-next-state
   #:simple-reader-initial-token
   #:simple-reader-process-token
   #:simple-reader-error
   ;; state macros
   #:defstate
   #:reader-state
   ;; base state types
   #:initial-state
   #:terminate-state
   #:ignore-state
   #:immediate-error-state
   #:intermediate-state
   #:unread-state
   #:whitespace-state
   #:macro-state
   #:terminating-macro-state
   #:invalid-char-state))

(in-package :generic-reader.simple-reader)

(eval-when (:compile-toplevel :execute)
  (series::install))

(defclass simple-reader ()
  ())

(defmethod readtablep ((object simple-reader))
  t)

;;;;
;;;; GENERIC FUNCTIONS
;;;;

(defgeneric simple-reader-char-syntax (readtable char)
  (:documentation "Returns an object describing the general syntactic
type of CHAR, which is passed to SIMPLE-READER-NEXT-STATE. Provided only
as a convenience: the default method simply returns CHAR.")
  (:method ((readtable simple-reader) char)
           char))

(defgeneric simple-reader-next-state (readtable state syntax)
  (:documentation "Returns the next state given that the reader has read
a character with the given syntactic type while in the given state."))

(defgeneric simple-reader-initial-token (readtable)
  (:documentation "Returns the token to be passsed to
SIMPLE-READER-ACCUMULATE-TOKEN when the first character of a token is
encountered."))

(defgeneric simple-reader-accumulate-token (readtable
                                            token char
                                            prev-state syntax state)
  (:documentation "Returns the token resulting from seeing the given
character after the given token has been accumulated. The reader state
before seeing the given character, the character's syntactic type, and
the state after seeing the character are passed for your dispatching
convenience."))

(defgeneric simple-reader-process-token (readtable token state)
  (:documentation "Returns the object to be returned as the result of
the read operation after accumulating the given token with the given
state just before reading the character that terminated the token."))

(defgeneric simple-reader-error (readtable prev-state char state stream)
  (:documentation "Signals an error resulting from reading the given
character after being in the given previous state, resulting in the
given state, which is a subtype of IMMEDIATE-ERROR-STATE."))

;;;;
;;;; READER STATE
;;;;

(defmacro defstate (name superclasses documentation)
  `(defclass ,name ,superclasses
    ()
    (:documentation ,documentation)))


(defstate initial-state ()
  "The reader starts out in this state.")

(defstate terminate-state ()
  "If there is a token being accumulated, this state terminates the
accumulation.")

(defstate ignore-state ()
  "This character is not accumulated into any tokens.")

(defstate immediate-error-state ()
  "Entering this state is an unconditoinal error.")

(defstate intermediate-state ()
  "EOF directly after this state is an error.")

(defstate unread-state (terminate-state)
  "This character should terminate a token as well as be unread, as it
is syntactically significant.")

(defstate whitespace-state (ignore-state terminate-state)
  "The character is ignored and terminates any token being
accumulated. It is also unread if we are preserving whitespace.")

(defstate macro-state ()
  "If no token is being accumulated, this state causes a function to be
called, which determines the result of the read operation.")

(defstate invalid-char-state (immediate-error-state)
  "An error is signalled indicating that a character which is invalid
for the current syntactic context has been read.")


(defmethod simple-reader-error ((readtable simple-reader)
                                prev-state char (state immediate-error-state)
                                stream)
  (error 'reader-error :stream stream))

(define-condition invalid-char-error (reader-error)
  ((char :initarg :char :initform (error "Initarg :CHAR is required")
         :reader invalid-char-error-char)
   (prev-state :initarg :prev-state :initform (error "Initarg :PREV-STATE is required")
               :reader invalid-char-error-prev-state))
  (:report
   (lambda (c s)
     (format s "The character ~A is invalid in the current syntactic context in stream ~A (reader state ~A)."
             (invalid-char-error-char c)
             (stream-error-stream c)
             (class-name (invalid-char-error-prev-state c))))))

(defmethod simple-reader-error ((readtable simple-reader)
                                prev-state char (state invalid-char-state)
                                stream)
  (error 'invalid-char-error :char char :prev-state prev-state :stream stream))

(defun class-prototype (name)
  #.(first '(#+cmu (mop:class-prototype (pcl::find-class name))
             #+sbcl (sb-pcl:class-prototype (sb-pcl::find-class name))
             #+lispworks (clos:class-prototype (find-class name))
             (make-instance name))))

(defmacro reader-state (name)
  `(class-prototype ',name))

;;;;
;;;; READER IMPLEMENTATION
;;;;

(defmacro do-cotruncating-when ((args &body test) &body body)
  `(multiple-value-bind ,args (until (mapping (,@(mapcar (lambda (x) `(,x ,x))
                                                         args))
                                       (declare (ignorable ,@args))
                                       ,@test)
                                     ,@args)
    ,@body))

(defmethod read-using-readtable ((readtable simple-reader) stream
                                 eof-error-p eof-value recursivep preserve-whitespace-p)
  "Initial state is (READER-STATE INITIAL-STATE). Loops reading
characters. If the current state is a subclass of INTERMEDIATE-STATE, T
is passed to the intial READ-CHAR for the EOF-ERROR-P argument. If the
current state is a subclass of INTIAL-STATE, the values for EOF-ERROR-P
and EOF-VALUE passed to READ-USING-READTABLE are passed on to
READ-CHAR. Otherwise, NIL is passed for both arguments.

SIMPLE-READER-CHAR-SYNTAX is called to determine the syntax treatment of
the character. The result of this, along with the current reader state,
is passed to SIMPLE-READER-NEXT-STATE. This function returns the actions
to be taken and the new state of the reader.

Possible actions:
call function (only valid if no token is accumulating), add to token, ignore, signal error, eof

if adding to token or ignoring:
terminate token and don't consume, terminate token and consume, continue accumulating token

If the char is accumulated into the token,
SIMPLE-READER-ACCUMULATE-TOKEN is called with the previously accumulated
token, previous state, current syntax, current char, and next state. The
initial token is the result of SIMPLE-READER-INITIAL-TOKEN, which is
called each time a token starts to be accumulated.

If a token has been accumulated, SIMPLE-READER-PROCESS-TOKEN is called
with the accumulated token and the state just entered."
  (let* ((char (scan-stream stream))
         (syntax (map-fn 't (lambda (char)
                              (simple-reader-char-syntax readtable char))
                         char)))
    (multiple-value-bind (prev-state state)
        (collecting-fn '(values t t)
                       (constantly nil (reader-state initial-state))
                       (lambda (prev-prev-state prev-state syntax)
                         (declare (ignore prev-prev-state))
                         (values prev-state
                                 (simple-reader-next-state readtable prev-state syntax)))
                       syntax)
      (multiple-value-bind (token-p token)
          (collecting-fn '(values boolean t)
                         (constantly nil (simple-reader-initial-token readtable))
                         (lambda (token-p token prev-state syntax char state)
                           (if (typep state 'ignore-state)
                               (values token-p token)
                               (values t (simple-reader-accumulate-token
                                          readtable
                                          token char
                                          prev-state syntax state))))
                         prev-state syntax char state)
        (do-cotruncating-when ((char prev-state state token-p token)
                               (and token-p (typep state 'terminate-state)))
          (iterate ((prev-state prev-state)
                    (state state)
                    (char char))
                   (when (typep state 'immediate-error-state)
                     (simple-reader-error readtable prev-state char state stream)))
          (cond
            ((typep (collect-last state) 'macro-state)
             (funcall (simple-reader-macro-char-processor readtable)
                      stream (collect-last char)))
            ((typep (collect-last prev-state) 'intermediate-state)
             (error 'end-of-file :stream stream))
            (t (if (collect-last token-p)
                   (prog1
                       (simple-reader-process-token
                        readtable (collect-last token) (collect-last prev-state))
                     ;; Is this code correct? CLHS for READ says:
                     ;; "read-preserving-whitespace is exactly like read
                     ;; when the recursive-p argument to
                     ;; read-preserving-whitespace is true." What does
                     ;; that mean? Does read-p-ws not preserve whitespace
                     ;; if it's recursive? The slash-reader example given
                     ;; would indicate otherwise. -- Rahul Jain
                     (when (or (and (or recursivep preserve-whitespace-p)
                                    (typep (collect-last state) 'whitespace-state))
                               (typep (collect-last state) 'unread-state)))
                     (unread-char stream (collect-last char)))
                   (if eof-error-p
                       (error 'end-of-file :stream stream)
                       eof-value)))))))))

