        
(defpackage :generic-reader
  (:export #:read
           #:read-preserving-whitespace
           #:read-delimited-list
           #:read-using-readtable
           #:read-from-string)
  (:shadow #:read
           #:read-preserving-whitespace
           #:read-delimited-list
           #:read-from-string)
  (:use :cl))
