;;; -*- Lisp -*-

(asdf:defsystem generic-reader
  :version 0.0
  :components ((:file "package")
               (:file "generic-reader" :depends-on ("package"))
               (:file "simple-reader" :depends-on ("generic-reader"))
               (:file "lisp-reader" :depends-on ("simple-reader")))
  :depends-on (series))
