(in-package :generic-reader)

(defun char-type-size (type)
  ;; need to check based on type?
  (declare (ignore type))
  char-code-limit)

(define-condition internal-reader-error (reader-error)
  ())

(defgeneric read-using-readtable (readtable stream eof-error-p eof-value recursivep preserve-whitespace-p)
  (:method ((readtable readtable) stream eof-error-p eof-value recursivep preserve-whitespace-p)
           (let ((*readtable* readtable))
             (if preserve-whitespace-p
                 (cl:read-preserving-whitespace stream eof-error-p eof-value recursivep)
                 (cl:read stream eof-error-p eof-value recursivep)))))

(defun read (&optional (stream *standard-input*)
                       (eof-error-p t)
                       (eof-value nil)
                       (recursive-p nil))
  (read-using-readtable *readtable* stream eof-error-p eof-value recursivep nil))

(defun read-preserving-whitespace (&optional (stream *standard-input*)
                                             (eof-error-p t)
                                             (eof-value nil)
                                             (recursive-p nil))
  (read-using-readtable *readtable* stream eof-error-p eof-value recursivep t))

(defgeneric readtablep (object)
  (:method ((object t)) nil)
  (:method ((object readtable)) t))

(defgeneric read-delimited-list-using-readtable (readtable stream char recursivep)
  (:method ((readtable readtable) stream char recursivep)
           (let ((*readtable* readtable))
             (cl:read-delimited-list char stream recursivep))))

(defun read-delimited-list (char &optional (stream *standard-input*) (recursivep nil))
  (read-delimited-list-using-readtable *readtable* stream char recursivep))

(defun read-from-string (string &optional (eof-error-p t) (eof-value nil)
                                &key (start 0) (end nil) (preserve-whitespace nil))
  (with-input-from-string (stream string :start start :end end)
    (read-using-readtable *readtable* stream eof-error-p eof-value nil preserve-whitespace)))