(defpackage :generic-reader.lisp-reader
    (:use :cl :generic-reader :generic-reader.simple-reader))

(defclass searching-for-token (intermediate-state) ())
(defclass read-eof (complete-state) ())
(defclass read-invalid-char (immediate-error-state) ())
(defclass read-incomplete () ())
(defclass reading-single-escape (symbol-state intermediate-state) ())
(defclass reading-multiple-escape (symbol-state intermediate-state) ())
(defclass reading-dispatched-macro () ())
(defclass reading-delimited-list () ())
(defclass reading-list () ())
(defclass reading-list-tail () ())
(defclass read-macro-char (macro-state) ())
(defclass reading-dispatching-macro-arg (intermediate-state) ())
(defclass read-dispatching-macro (macro-state) ())

(defclass invalid-char () ())
(defclass eof-char () ())
(defclass symbol-consituent-char () ())
(defclass numeric-char (symbol-consituent-char) ())
(defclass decimal-char (numeric-char))
(defclass float-fraction-delimiter () ())
(defclass float-exponent-char () ())
(defclass ratio-delimiter () ())
(defclass package-delimiter () ())
(defclass multiple-escape-char () ())
(defclass single-escape-char () ())
(defclass whitespace-char () ())
(defclass macro-char () ())
(defclass dispatching-macro-char () ())

(defmacro deftransition (prev-state char-type next-state &optional docstring)
  `(defmethod simple-reader-next-state ((readtable lisp-readtable)
                                        (prev-state ,prev-state)
                                        (char ,char-type))
    ,@(and docstring (list docstring))
    (reader-state ,next-state)))


(deftransition t invalid-char
  read-invalid-char)
(deftransition intermediate-state eof-char
  read-incomplete)

(deftransition searching-for-token macro-char
  read-macro-char)
(deftransition searching-for-token dispatching-macro-char
  reading-dispatching-macro-arg)
(deftransition reading-dispatching-macro-arg decimal-char
  reading-dispatching-macro-arg)
(deftransition reading-dispatching-macro-arg t
  read-dispatching-macro)

(deftransition t single-escape-char
  reading-single-escape)
(deftransition reading-single-escape t
  reading-)

(deftransition searching-for-token numeric-char
  reading-integer-digits)
(deftransition searching-for-token float-fraction-delimiter
  reading-float-fraction)
(deftransition reading-decimal-digits decimal-char
  reading-decimal-digits)
(deftransition reading-decimal-digits float-fraction-delimiter
  reading-float-fraction)
(deftransition reading-float-fraction )
